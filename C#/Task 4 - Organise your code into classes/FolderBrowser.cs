﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace FolderBrowser
{
    class FolderBrowser
    {

        private FolderInfo fi;

        static void Main(String[] args)
        {
            FolderInfo folder = new FolderInfo(Environment.GetFolderPath(
            Environment.SpecialFolder.MyDocuments));

            FolderBrowser browser = new FolderBrowser(folder);

            while (true)
            {
                Console.Clear();
                ColorConsole.WriteLine(folder.GetDescription());

                string[] subFolders = browser.GetSubFolders();
                for (int i = 0; i < subFolders.Length; i++)
                {
                    string dirName = new DirectoryInfo(subFolders[i]).Name;
                    ColorConsole.WriteLine(String.Format("{0}\t{1}", i, dirName));
                }

                ColorConsole.WriteLine(
                   "Type a subfolder number or press Enter to move to the parent folder.",
                    ConsoleColor.DarkGray);
                ColorConsole.Write("> ");
                string input = Console.ReadLine();
                if (String.IsNullOrWhiteSpace(input))
                {
                    browser.MoveUpToParentFolder();
                    folder = browser.GetCurrentFolder();
                }
                else
                {
                    int selection = Int32.Parse(input);
                    folder = new FolderInfo(subFolders[selection]);
                    browser = new FolderBrowser(folder);
                }
            }
        }

        public FolderBrowser(FolderInfo fi)
        {
            this.fi = fi;
        }

        public FolderInfo GetCurrentFolder()
        {
            return this.fi;
        }

        public string[] GetSubFolders()
        {
            return Directory.GetDirectories(fi.GetPath());
        }

        public void MoveUpToParentFolder()
        {
            this.fi = new FolderInfo(Directory.GetParent(fi.GetPath()).ToString());
        }

    }

    class FolderInfo
    {
        string path;
        DirectoryInfo di;
        
        public FolderInfo(string path) 
        {
            this.path = path;
            this.di = new DirectoryInfo(path);
        }

        public string GetDescription()
        {
            return di.GetFiles().Length+" files ("+GetSizeOfFiles()+" bytes), "+di.GetDirectories().Length+" folders";
        }

        public long GetSizeOfFiles()
        {
            long bytes = 0;
            // assumes an instance field called FolderPath
            foreach (string file in Directory.EnumerateFiles(this.path))
            {
                bytes += new FileInfo(file).Length;
            }
            return bytes;
        }

        public string GetPath()
        {
            return path;
        }

    }
    
    class ColorConsole {

        public static void Write(string msg, ConsoleColor clr = ConsoleColor.Gray)
        {
            ConsoleColor prev_color = Console.ForegroundColor;
            Console.ForegroundColor = clr;
            Console.Write(msg);
            Console.ForegroundColor = prev_color;
        }

        public static void WriteLine(string msg, ConsoleColor clr = ConsoleColor.Gray)
        {
            Write(msg + "\n", clr);
        }

        public static void RainbowLine(string msg)
        {
            ConsoleColor prev = Console.ForegroundColor;
            int i = 0;
            foreach (char c in msg)
            {
                Console.ForegroundColor = (ConsoleColor.Gray + i);
                Console.Write(c);
                i = (i + 1) % 6;
            }
            Console.ForegroundColor = prev;
            Console.WriteLine();
        }

        static void WriteLine(string description)
        {
            throw new NotImplementedException();
        }
    }
    
}
