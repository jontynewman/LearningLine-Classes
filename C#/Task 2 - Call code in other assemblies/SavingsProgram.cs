﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using LearningLine.Practice;

namespace ConsoleApplication
{
    class SavingsProgram
    {
        static void Main(string[] args)
        {
            SavingsProgram program = new SavingsProgram();
            program.run();
        }

        public void run()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Println(ConfigurationManager.AppSettings["ApplicationName"]);
            Println();
            
            CalcSequence();
            Console.ForegroundColor = ConsoleColor.Gray;

            WaitForPrompt("Press any key to quit... ");
        }

        private void WaitForPrompt(String str)
        {
            Console.Write(str);
            Console.ReadKey();
        }

        private Boolean CalcSequence()
        {
            Double saving;
            int years;
            Double rate;
            string formatInput = "{0,37}";
            string formatOutput = "{0,-26}{1,0}";

            Console.ForegroundColor = ConsoleColor.Gray;
            saving = DemandNumber(String.Format(formatInput,
                    "Enter a monthly savings amount (£): "));
            years = (int)DemandNumber(String.Format(formatInput, 
                    "Enter the # of years: "));
            rate = DemandNumber(String.Format(formatInput, 
                    "Enter the annual interest rate (%): "));
            Println();

            Double rate_monthly = Math.Round(rate/12,4);
            int months = years*12;

            Console.ForegroundColor = ConsoleColor.Yellow;
            Println(String.Format(formatOutput,
                    "Monthly Payment:", saving.ToString("£0.00")
                ));
            Println(String.Format(formatOutput,
                    "Monthly Interest Rate:", rate_monthly.ToString("0.0000 \\%")
                ));
            Println(String.Format(formatOutput,
                    "Duration:", months.ToString("0 months")
                ));
            Println(String.Format(formatOutput, 
                    "Future Value:", 
                    SavingsCalc.CalculateFutureValue(saving, months, rate_monthly/100).ToString("£0.00")
                ));
            Println();
            return true;
        }

        private Double DemandNumber(String command)
        {
            Double number = 0;
            Boolean valid = false;
            while (!valid)
            {
                try
                {
                    number = Double.Parse(Ask(command));
                    valid = true;
                }
                catch (FormatException)
                {
                    Println("Could not convert input to a number.");
                }
                catch (OverflowException)
                {
                    Println("The number entered caused overflow to occur.");
                }
            }
            return number;
        }

        private String Ask(String question)
        {
            Console.Write(question);
            return Console.ReadLine();
        }

        private void Println(String str)
        {
            Console.WriteLine(str);
        }

        private void Println()
        {
            Console.WriteLine();
        }
    }
}
