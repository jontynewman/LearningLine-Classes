﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication
{
    class DaysAgo
    {
        
        static void Main(string[] args)
        {
            DateTime date = new DateTime(2012, 4, 10);

            for (int i = 0; i < 15; i++)
            {
                date = date.AddDays(-i * i);
                Console.WriteLine("{0} was {1}.", date.ToShortDateString(), GetDaysAgo(date));
            }
            PressSomeKeyToContinue();
        }
        

        static string GetDaysAgo(DateTime date)
        {
            // We are hard coding the start do for consitent output.
            // In reality you would probably use DateTime.Now
            DateTime todayDate = new DateTime(2012, 4, 10);

            double daysAgo = todayDate.Subtract(date.Date).Days;
            int weeksAgo = (int)(daysAgo / 7.0);
            int monthsAgo = (int)(daysAgo / 30.0);
            int yearsAgo = (int)(daysAgo / 365.0);

            string measure = (yearsAgo==0)?((monthsAgo==0)?((weeksAgo==0)?"day":"week"):"month"):"year";
            int ago = (yearsAgo==0)?((monthsAgo==0)?((weeksAgo==0)?(int)Math.Floor(daysAgo):weeksAgo):monthsAgo):yearsAgo;

            string readable_format = "";
            bool special_format = false;

            if (measure == "day")
            {
                if (ago <= 1)
                {
                    special_format = true;
                    if (ago < 1) readable_format = "today";
                    else readable_format = "yesterday";
                }
            }

            if(!special_format)
            {
                readable_format = ago+" "+((ago>1)?measure+"s":measure);
            }
            return readable_format;
        }

        
        static void PressSomeKeyToContinue()
        {
            Console.ReadKey();
        }

    }
}
