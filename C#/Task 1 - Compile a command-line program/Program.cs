﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using LearningLine.Practice;

namespace ConsoleApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.run();
        }

        public void run()
        {
            Println("This Day In History");
            Println("Today's date is " + DateTime.Today.ToShortDateString() + ".");
            Println();

            Println("Welcome, " + Ask("Login: ").ToUpper() + ".");
            Println();

            while (!SquareAndCubeSequence()) ;
            while (!BirthdateSequence()) ;

            WaitForPrompt("Press any key to quit... ");
        }

        private void WaitForPrompt(String str)
        {
            Console.Write(str);
            Console.ReadKey();
        }

        private Boolean SquareAndCubeSequence() 
        {
            Boolean valid = false;
            try {
                Double number = Double.Parse(Ask("Enter a number: "));
                String tableFormat = "{0,10}{1,10},{2,10}";
                String numberFormat = "0.0000";
                Println(String.Format(tableFormat, "num", "num^2", "num^3"));
                Println(String.Format(tableFormat, 
                        number.ToString(numberFormat),
                        (Math.Pow(number, 2)).ToString(numberFormat),
                        (Math.Pow(number, 3)).ToString(numberFormat)
                        ));

                valid = true;
            }
            catch(FormatException)
            {
                Println("Could not convert input to a number.");
            }
            catch(OverflowException)
            {
                Println("The number entered caused overflow to occur.");
            }
            Println();
            return valid;
        }

        private Boolean BirthdateSequence()
        {
            Boolean valid = false;
            try
            {
                DateTime birthdate = DateTime.Parse(Ask("What is your birthdate? "));

                Println("You are " + Math.Floor((DateTime.Now-birthdate).TotalDays) + " days old.");
                Println("You were born on a "+birthdate.DayOfWeek+".");
                Boolean isLeapYear = CultureInfo.CurrentCulture.Calendar.IsLeapYear(birthdate.Year);
                Println(birthdate.Year+((isLeapYear)?" was":" was *not*")+" a leap year.");
                Println();

                Println("On "+birthdate.ToString("MMMM d")+" in history: ");
                Println(TodayInHistoryDb.GetEventFromDay(birthdate.Month, birthdate.Day));

                valid = true;
            }
            catch (FormatException)
            {
                Println("Could not interpret string as a date.");
                Println("Try formatting your input as \"<month> <day>, <year>\".");
                Println("For example: January 1, 1970");
            }
            Println();
            return valid;
        }

        private String Ask(String question)
        {
            Console.Write(question);
            return Console.ReadLine();
        }

        private void Println(String str)
        {
            Console.WriteLine(str);
        }

        private void Println()
        {
            Console.WriteLine();
        }
    }
}
