class Graph:
	"""
	A directed graph.

	Implement it as an adjacency list. A dictionary should map nodes
	to a set of nodes. An edge is a tuple: (from_node, to_node).
	"""

	def __init__(self):
		"""Initialize the graph data structure."""
		self.adj_list = dict()
	
	def add_node(self, node):
		"""Add a node to the graph, but there's no edges from it"""
		if node in self.adj_list.keys(): 
			raise NodeExistsException('The specified node already exists.')
		self.adj_list[node] = []
		

	def add_edge(self, edge):
		"""An edge is a tuple of nodes. If the nodes don't exist, create
		them."""
		try: self.add_node(edge[0])
		except NodeExistsException: pass
		try: self.add_node(edge[1])
		except NodeExistsException: pass
		
		self.adj_list[edge[0]].append(edge[1])

	def remove_node(self, node):
		"""Remove a node and all edges to and from it."""
		try:
			self.adj_list.pop(node)
			for n,edges in self.adj_list.items():
				for i,edge in enumerate(edges):
					if edge == node:
						edges.pop(i)
		except KeyError:
			raise NodeNonexistentException( '' /
				+ 'Cannot remove a node that does not exist.')
	
	def remove_edge(self, edge):
		try:
			edges = self.adj_list[edge[0]]
			edges.pop(edges.indexOf(edge[1]))
		except KeyError:
			raise NodeNonexistentException( '' /
				+ 'There weren\'t any ' /
				+ 'edges containing one or more of ' /
				+ 'the specified nodes.')
		
	def nodes(self):
		"""Return a generator of all the nodes."""
		for node,edges in self.adj_list.items():
			yield node

	def edges(self):
		"""Return a generator of all edge tuples."""
		for node,edges in self.adj_list.items():
			for edge in edges:
				yield (node,edge)

	def __contains__(self, node):
		"""Support the "in" operator. Return whether node is in the graph."""
		return node in self.adj_list.keys()

	def __getitem__(self, node):
		"""Support graph[node] syntax. Return a list of edges."""
		return self.adj_list[node]
		
	def __str__(self):
		string = ''
		for node,edges in self.adj_list.items():
			string = string + node + ' => ['
			for i,edge in enumerate(edges):
				string = string + edge
				if i < len(edges)-1: string = string + ', '
			string = string + '] \n'
		return string

class NodeExistsException(Exception):
	pass
	
class NodeNonexistentException(Exception):
	pass

def main(args):
	graph = Graph()
	
	help(graph)
	
	print("Initialising graph...")
	graph.add_node('A')
	graph.add_node('B')
	graph.add_node('C')
	graph.add_edge(('A','B'))
	graph.add_edge(('A','C'))
	graph.add_edge(('A','D'))
	graph.add_edge(('B','C'))
	graph.add_edge(('B','D'))
	graph.add_edge(('C','A'))
	graph.add_edge(('C','D'))
	graph.add_edge(('D','A'))
	print(graph)
	
	print("Removing node D... ")
	graph.remove_node(('D'))
	print(graph)
	
	print("Fetching all nodes... ")
	for node in graph.nodes(): print(node)
	print()
	
	print("Fetching all edges... ")
	for edge in graph.edges(): print(edge)
	print()
	
	print("'A' in graph => ",end="")
	print('A' in graph)
	print("'D' in graph => ",end="")
	print('D' in graph)
	print()
	
	print("graph['A'] => ",end="")
	print(graph['A'])

if __name__ == '__main__':
	import sys
	main(sys.argv)
	

