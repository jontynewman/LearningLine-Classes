def permutations(word):
	perms = []
	length = len(word)
	
	# Stopping condition called once the word is 
	# reduced to a single character or less.
	if length<=1:
		perms.append(word)
	else:
		# For each unique character in the word...
		for i in range(length):
			# Calculate the permutations of every other character.
			tails = permutations(word[0:i]+word[i+1:])
			# Then append these permutations to the character 
			# that was not included in the calculation,
			# adding them to the list of total permutations.
			for tail in tails:
				perms.append(word[i]+tail)
	# Return a list without any duplicates
	# Note: room for optimization here 
	# as there is no need to calculate duplicates. 
	return list(set(perms));

def main(word):
	# Display all permutations of the specified word.
	print(permutations(word))

if __name__ == '__main__':
	import sys
	word = 'ab' # Default value
	# If a word is specified, overwrite the default value.
	if len(sys.argv) > 1:
		word = sys.argv[1]
	# Display all permutations of the word.
	main(word)
	

