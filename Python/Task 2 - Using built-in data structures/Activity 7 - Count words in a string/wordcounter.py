import re

def wordCounter(text):
	tally = {}
	top10 = []
	words = re.findall('[a-zA-Z]+', text);
	# Count the number of times each word is repeated.
	for word in words:
		word = word.lower()
		if word in tally: tally[word] = tally[word]+1
		else: tally[word] = 1
	# Extract the 10 most frequently used words	
	tally = sorted(tally.items(), key=lambda word: -word[1])[:10]
	
	# Uncomment the line below to return a list of 
	# tuples with the word and its frequency.
	#return tally;
	
	# Extract each word from the tuples.
	for tup in tally:
		top10.append(tup[0])
	return top10
	

def main(word):
	# Display all permutations of the specified word.
	print(wordCounter(word))

if __name__ == '__main__':
	import sys
	# Default value
	text = "Four score and seven years ago our fathers brought forth on this continent, a new nation, conceived in Liberty, and dedicated to the proposition that all men are created equal. Now we are engaged in a great civil war, testing whether that nation, or any nation so conceived and so dedicated, can long endure. We are met on a great battle-field of that war. We have come to dedicate a portion of that field, as a final resting place for those who here gave their lives that that nation might live. It is altogether fitting and proper that we should do this. But, in a larger sense, we can not dedicate -- we can not consecrate -- we can not hallow -- this ground. The brave men, living and dead, who struggled here, have consecrated it, far above our poor power to add or detract. The world will little note, nor long remember what we say here, but it can never forget what they did here. It is for us the living, rather, to be dedicated here to the unfinished work which they who fought here have thus far so nobly advanced. It is rather for us to be here dedicated to the great task remaining before us -- that from these honored dead we take increased devotion to that cause for which they gave the last full measure of devotion -- that we here highly resolve that these dead shall not have died in vain -- that this nation, under God, shall have a new birth of freedom -- and that government of the people, by the people, for the people, shall not perish from the earth."
	# If a word is specified, overwrite the default value.
	if len(sys.argv) > 1:
		text = sys.argv[1]
	# Display word count and top 10 words.
	main(text)
	

