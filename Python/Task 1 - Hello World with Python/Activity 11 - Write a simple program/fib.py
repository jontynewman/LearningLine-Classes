def fib(n):
	# Calculate Fibonacci sequence recursively
	if n > 1:
		result = fib(n-1) + fib(n-2);
	# Terminating conditions
	elif n == 1:
		result = 1
	else:
		result = 0 # default value
	return result

def main(val):
	# Output Fibonnaci sequence in the required format
	print("Fibonacci of",val,"is",fib(val))

if __name__ == '__main__':
	import sys
	val = '0' # Default value
	# If an argument is specified, overwrite the default value.
	if len(sys.argv) > 1:
		val = sys.argv[1]
	# Output the value's factorial.
	main(int(val))
	

