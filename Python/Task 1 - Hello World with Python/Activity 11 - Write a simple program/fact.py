def fact(n):
	# Calculate Factorial number recursively
	if n > 1:
		result = n * fact(n-1);
	# Terminating conditions
	elif n == 1:
		result = 1
	else:
		result = 0 # default value
	return result

def main(val):
	# Output Factorial number in the required format
	print("Factorial of",val,"is",fact(val))

if __name__ == '__main__':
	import sys
	val = '0' # Default value
	# If an argument is specified, overwrite the default value.
	if len(sys.argv) > 1:
		val = sys.argv[1]
	# Output the value's factorial.
	main(int(val))
	

