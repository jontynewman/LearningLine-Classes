import fact
import fib

def main(val):
	print("Output of",val) # Display the specified value
	fact.main(val) # Calculate and output Factorial number
	fib.main(val) # Calculate and output Fibonacci sequence

if __name__ == '__main__':
	import sys
	val = '0' # Default value
	# If an argument is specified, overwrite the default value.
	if len(sys.argv) > 1:
		val = sys.argv[1]
	# Output the value's factorial.
	main(int(val))
	

