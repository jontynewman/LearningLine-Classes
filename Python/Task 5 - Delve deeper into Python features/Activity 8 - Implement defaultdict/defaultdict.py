class MyDictionary(dict):
	"""dict() -> new empty dictionary
	dict(mapping) -> new dictionary initialized from a mapping object's
		(key, value) pairs
	dict(iterable) -> new dictionary initialized as if via:
		d = {}
		for k, v in iterable:
			d[k] = v
	dict(**kwargs) -> new dictionary initialized with the name=value pairs
		in the keyword argument list.  For example:  dict(one=1, two=2)
	dict(default_factory=x) -> new dictionary initialized that will
		default to value x if a KeyError occurs
	dict(mapping, default_factory=x) -> new dictionary initialized from 
		a mapping object's (key, value) pairs that will default to value
		x if a KeyError occurs
	dict(iterable, default_factory=x) -> new dictionary initialized that
		will default to value x if a KeyError occurs as if via:
		d = {}
		for k, v in iterable:
			d[k] = v
	dict(default_factory=x,**kwargs) -> new dictory initialized with the
		name=value pairs in the keyword argument list that will default 
		to value x if a KeyError occurs. 
		For example: dict(default_factory=x, one=1, two=2)"""
	def __init__(self, *args, default_factory=None, **kwargs): 
		"""x.__init__ initializes x; 
		keys that do not have a value yet associated with them 
		will return the default_factory value if specified"""
		super(*args,**kwargs)
		self.default_factory = default_factory

	def __getitem__(self, key):
		"""x.__getitem__(y) <==> x[y]
		The returned value will default to the default_factory value 
		if specified during initialization"""
		try: 
			return super(MyDictionary,self).__getitem__(key)
		except KeyError as key_error:
			if self.default_factory:
				return self.default_factory()
			else:
				raise key_error

def main(args):
	print("Initialising MyDictionary " \
		+ "with default value 0... ", end="")
	default_dict = MyDictionary(default_factory=lambda:0)
	print("default_dict initialised! \n")
	
	print('Assigning "five" to default_dict[5]... ', end="")
	default_dict[5] = "five"
	print("success!")
	print("default_dict[5] =>",default_dict[5],"\n")
	
	print("Calling default_dict[0], 0 expected... ", end="")
	value = default_dict[0]
	success = "success!" if value==0 else "failure."
	print(success)
	print("default_dict[0] =>",value,"\n")
	
	print("Initialising MyDictionary " \
		+ "without a default value... ", end="")
	non_default = MyDictionary()
	print("non_default initialised! \n")
	
	print('Assigning "F1V3" to non_default[5]... ', end="")
	non_default[5] = "F1V3"
	print("success!")
	print("non_default[5] =>",non_default[5],"\n")
	
	print("Calling non_default[0], KeyError expected... ", end="")
	success = "success!"
	try: value = non_default[0]; success = "failure."
	except KeyError: value = "KeyError"
	print(success)
	print("non_default[0] =>",value,"\n")
	
	input("Press enter to view documentation... ")
	help(MyDictionary)

if __name__ == '__main__':
	import sys
	main(sys.argv)
	

