def rainbow_table(length):
	import hashlib
	
	# Yield for the number of possible combinations.
	for possibility in word_combos(length):
		hash_token = hashlib.sha256(possibility.encode()).digest()
		yield (possibility,hash_token)

def following_letters(begin='z'):
	# Find all characters greater than the specified character.
	# (by default, the value is 'z' so that 
	# the generator starts from 'a')
	next_letter = ord(begin)
	a = 0x61
	z = 0x7A
	while True:
		# If the current letter is 'z', then loop back to 'a'.
		# Otherwise, simply return the next letter.
		next_letter = next_letter + 1 if next_letter < z else a
		yield chr(next_letter)

def word_combos(length):
	letters = ['a' for i in range(length)]
	# Firstly, yield the initial word combination.
	yield ''.join(letters)
	# Then begin determining all other possible combinations.
	for i in range(pow(26,length)-1):
		# 'Increment' and yield the next word combination
		for i,letter in enumerate(letters):
			letters[i] = next(following_letters(begin=letter))
			# If an increment of trailing characters is needed,
			# continue to do so as required.
			if letter < 'z': break
		yield ''.join(letters[::-1])
			
			
def main(length):
	# Display the rainbow table.
	for possibly in rainbow_table(length):
		print(possibly[0],'=>',possibly[1],end='\n\n')

if __name__ == '__main__':
	import sys
	# Default value
	length = 8
	# If a length is specified, overwrite the default value.
	if len(sys.argv) > 1:
		length = int(sys.argv[1])
	# Display rainbox table for 
	main(length)
	

