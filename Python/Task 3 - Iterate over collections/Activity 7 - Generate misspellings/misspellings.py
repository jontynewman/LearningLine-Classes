def split_word(word):
	return [(word[0:split],word[split:]) for split in range(len(word)+1)]
	
def delete1_word(word):
	return [split_word[0]+split_word[1][1:] for split_word in split_word(word)[:-1]]
	
def transpose1_word(word):
	return [split_word[0]+split_word[1][1]+split_word[1][0]+split_word[1][2:] for split_word in split_word(word)[:-2]]
	
def replace1_word(word):
	replaced = []
	for i,letter in enumerate(word):
			replaced += [word[0:i]+alpha+word[i+1:] for alpha in alphabet(letter)]
	return replaced
	
def insert1_word(word):
	replaced = []
	for i in range(len(word)+1):
			replaced += [word[0:i]+alpha+word[i:] for alpha in alphabet('a')]
	return replaced

def alphabet(start='a'):
	start = ord(start)
	current = start-1
	a = 0x61
	z = 0x7A
	end = start-1 if start > a else z 
	for i in range(26):
		current = a if current >= z else current+1
		yield chr(current) 

def main(word):
	print(split_word(word))
	print(delete1_word(word))
	print(transpose1_word(word))
	print(replace1_word(word))
	print(insert1_word(word))

if __name__ == '__main__':
	import sys
	# Default value
	word = 'abcd'
	# If a word is specified, overwrite the default value.
	if len(sys.argv) > 1:
		word = sys.argv[1]
	main(word)
	

